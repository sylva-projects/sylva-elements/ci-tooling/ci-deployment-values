# CI Deployment Values

This repository is used to store deployment values used by Sylva-core CI.

CI values are in different folders:

* a folder `common` for all values usable in any case
* a folder `current` for values usable on main and dev branches
* a folder per specific versions (currently `1.1.1`)

Sylva-core side implementation uses filenames to apply a given change:

* if a requested file exists in `common`, it will be applied first
* if the same file exists in specific version, it will also be applied afterwards

So CI values can be stored with the following logic:

* if a value file is suitable for any version, it can simply be stored in `common`
* if a value file is partially suitable for any version but some changes/patch are needed for some specific versions, the file can be put in `common` folder and a file with the same name containing only changes can be put in version folder(s)
* If a value file is only suitable for current version (main and dev branches) it can be store in `current` folder
* If a value file is completely different between current and previous versions (no easy value overwrite), files with same name but different values can be stored in each version folders.

Note: There is also a special file named `private-values-override.yaml` which can be used to override private values for a specific version.

## Add & Test a New Set of Values

To add or update a set of values, you need to create a new Merge Request (MR) in this repository.

Then you have multiple scenarios:

### If you are a Sylva-core maintainer

* If this change doesn't require any changes on the Sylva-core side (e.g., you change a value in an existing file), it can be tested directly in the MR via the cross-repo pipeline.
* If this change comes along with a Sylva-core MR, you can test it by manually running a pipeline on your dev branch and changing the `SYLVA_CORE_BRANCH` variable to target your branch.

### If you are NOT a Sylva-core maintainer

* Create a MR on Sylva-core and temporarily change the value of the `SYLVA_CI_VALUES_REVISION` variable. This variable is defined in the Sylva-core file: `.gitlab/ci/main-pipeline-deployment-tests.yml`. You can also override this value when you manually run a pipeline.
